const axios = require("axios").default;
const qs = require("qs");
const { dohAddr } = require("../config/config.js");

exports.resolveDoH = (domain, subnet) => {
  const param = qs.stringify({
    name: domain,
    edns_client_subnet: subnet,
  });
  return new Promise((resolve, reject) => {
    axios
      .get(dohAddr.quad9 + param)
      .then((response) => {
        resolve(response.data);
      })
      .catch((reason) => reject(reason));
  });
};
