const { Resolver } = require("dns");
const { dnsAddr } = require("../config/config.js");

function buildResolver(dnsServers) {
  const resolver = new Resolver();
  resolver.setServers(dnsServers);
  return resolver;
}

exports.resolveDns4 = (domain) => {
  const resolver = buildResolver(dnsAddr.local);

  return new Promise((resolve, reject) => {
    resolver.resolve4(domain, (err, records) => {
      if (err) {
        reject(err);
      } else {
        resolve(records);
      }
    });
  });
};
