const dns = require("native-dns");
const { resolveDoH } = require("./lib/doh.js");
const { subnet } = require("./config/config.js");

const server = dns.createServer();

const cache = new Map();

function buildAnswer(answer) {
  const res = { records: [], data: [] };
  if (answer) {
    answer.forEach((a) => {
      res.data.push(a.data);

      if (a.type === 1) {
        res.records.push(
          dns.A({
            name: a.name,
            address: a.data,
            ttl: 1,
          })
        );
      } else if (a.type === 5) {
        res.records.push(
          dns.CNAME({
            name: a.name,
            data: a.data,
            ttl: 1,
          })
        );
      }
    });
  }
  return res;
}

server.on("request", function (request, response) {
  const start = new Date();

  const ques = request.question[0];
  const log = { name: ques.name };

  if (cache.has(ques.name)) {
    response.answer.push(...cache.get(ques.name));
    response.send();
    log.time = new Date().getTime() - start.getTime();
    log.cache = true;
    console.log(JSON.stringify(log));
  } else {
    resolveDoH(ques.name, subnet)
      .then((res) => {
        if (res.Answer) {
          const ans = buildAnswer(res.Answer);
          log.time = new Date().getTime() - start.getTime();
          log.ip = ans.data;
          response.answer.push(...ans.records);
          cache.set(ques.name, ans.records);
        }
        response.send();
        console.log(JSON.stringify(log));
      })
      .catch((reason) => {
        console.log("Err: " + reason);
      });
  }
});

server.on("error", function (err, buff, req, res) {
  console.log(err.stack);
});

server.serve(53);
