exports.dohAddr = {
  ali: "https://dns.alidns.com/resolve?",
  quad9: "https://dns11.quad9.net:5053/dns-query?",
};

exports.dnsAddr = {
  local: ["192.168.3.127"],
  ali: ["223.5.5.5", "223.6.6.6"],
  dnsPod: ["119.29.29.29", "182.254.116.116"],
  google: ["8.8.8.8", "8.8.4.4"],
  cloudFlare: ["1.1.1.1", "1.0.0.1"],
  quad9: ["9.9.9.9", "149.112.112.112"],
};

exports.subnet = "219.137.229.0/24";
