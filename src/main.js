const { resolveDns4 } = require("./lib/dns.js");

const domain = "www.tmall.com";

resolveDns4(domain)
  .then((res) => {
    console.log(res);
  })
  .catch((reason) => {
    console.log(reason);
  });
